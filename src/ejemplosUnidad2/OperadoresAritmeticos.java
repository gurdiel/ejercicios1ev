package ejemplosUnidad2;

public class OperadoresAritmeticos 
{
	public static void main(String[] args)
	{
		short x = 7;
		int y = 5;
		float f1 = 13.5f;
		float f2 = 8f;
		System.out.println("El valor de x es " + x + ", el de y es " + y);
		System.out.println("El resultado de x + y es " + (x + y));
		System.out.println("El resultado de x - y es " + (x - y));
		System.out.printf("%s\n%s%s\n","Divisi�n entera:","x / y = ",(x/y));
		System.out.println("Resto de la divisi�n entera: x % y = " + (x % y));
		System.out.println("El valor de f1 es " + f1 + ", f2 es " + f2);
		System.out.println("El resultado de f1 / f2 es " + (f1 / f2));
	} // fin de main
} // fin de la clase operadoresaritmeticos
