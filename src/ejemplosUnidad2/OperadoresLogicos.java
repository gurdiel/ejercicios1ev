package ejemplosUnidad2;

public class OperadoresLogicos
{
	public static void main(String[] args) 
	{
		System.out.println("OPERADORES L�GICOS");
		System.out.println();
		System.out.println("Negacion:\n ! false es : " + (! false));
		System.out.println(" ! true es : " + (! true));
		System.out.println();
		System.out.println("Operador AND (&):\n false & false es : " + (false & false));
		System.out.println(" false & true es : " + (false & true));
		System.out.println(" true & false es : " + (true & false));
		System.out.println(" true & true es : " + (true & true));
		System.out.println();
		System.out.println("Operador OR (|):\n false | false es : " + (false | false));
		System.out.println(" false | true es : " + (false | true));
		System.out.println(" true | false es : " + (true | false));
		System.out.println(" true | true es : " + (true | true));
		System.out.println();
		System.out.println("Operador OR Exclusivo (^):\n false ^ false es : " + (false ^ false));
		System.out.println(" false ^ true es : " + (false ^ true));
		System.out.println(" true ^ false es : " + (true ^ false));
		System.out.println(" true ^ true es : " + (true ^ true));
		System.out.println();
		System.out.println("Operador &&:\n false && false es : " + (false && false));//Por eso nos avisa de quitarlo Siempre es false es un and
		System.out.println(" false && true es : " + (false && true));//Idem.
		System.out.println(" true && false es : " + (true && false));
		System.out.println(" true && true es : " + (true && true));
		System.out.println();
		System.out.println("Operador ||:\n false || false es : " + (false || false));
		System.out.println(" false || true es : " + (false || true));
		System.out.println(" true || false es : " + (true || false));//Imagino que da warning siempre es verdadero
		System.out.println(" true || true es : " + (true || true));//lo mismo al ser un or
  } // fin main
} // fin OperadoresLogicos