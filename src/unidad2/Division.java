package unidad2;
/*En el m�todo main de una clase Java llamada Division escribe un programa
que muestre en la consola la divisi�n de 1234 entre 532 siendo ambos n�meros
reales. El formato de salida ser� un n�mero que ocupar� un m�nimo de 15
caracteres en pantalla, de los cuales dos se utilizar�n para la parte decimal.**/


public class Division {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float a=1234;
		float b=532;
		float r=a/b;
		System.out.print(r);
		System.out.printf("1234 / 532 = %15.2f", 1234f / 532f);
		System.out.println("1234/532 = " + String.valueOf(1234f/532f));//conversi�n explicita
		
	}

}
