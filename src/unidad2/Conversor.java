package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); //creando objeto bufferedreader
		float euros=0;
		System.out.println("Euros:");
		String linea = in.readLine();
		
		euros= Float.parseFloat(linea);
		
		float dolares;
		dolares = euros * 1.11f;
		System.out.printf("Dolares: %.2f",dolares);
		
		//System.out.print("Euros: ");
		//System.out.printf("Dolares: %.2f", Float.parseFloat(in.readLine()) * 1.11f);
		
	}

}
