package unidad3;

import java.util.Scanner;

/*6. En el m�todo main de una clase Java llamada AdivinaNumero escribe un programa 
quefuncione como una calculadora. El usuario tendr� que elegir con el teclado la 
operaci�n que desea realizar (suma, resta, multiplicaci�n o dividisi�n) o salir 
del programa. Si elige una operaci�n, tendr� que introducir por teclado dos 
n�meros y a continuaci�n mostrar� el resultado de la operaci�n con el formato 
n�mero1 op n�mero2 = resultado, donde op ser� el s�mbolo que corresponda: 
+, -, x, /. Cada vez que se muestre resultado, el usuario podr� volver a elegir otra
operaci�n o salir.**/

public class Calculadora {

	public static void main(String[] args) {

		Scanner t = new Scanner(System.in);
		int operacion;

		System.out.println("CALCULADORA CIFP\n" + "------------------------------\n"
				+ "1.Suma\n2.Resta\n3.Multiplicaci�n\n4.Divisi�n\n0.Salir");
		operacion = t.nextInt();

		while (operacion != 0) {
			System.out.println("Introduzca primer operando");
			int num1 = t.nextInt();
			System.out.println("Introduzca segundo operando");
			int num2 = t.nextInt();

			switch (operacion) {
			case 1:
				System.out.printf("%d + %d = %d\n\n", num1, num2, (num1 + num2));
				break;
			case 2:
				System.out.printf("%d - %d = %d\n\n", num1, num2, (num1 - num2));
				break;
			case 3:
				System.out.printf("%d * %d = %d\n\n", num1, num2, (num1 * num2));
				break;
			case 4:
				System.out.printf("%d / %d = %d\n\n", num1, num2, (num1 / num2));
				break;
			default:
				System.out.printf("Adios");

			}
			System.out.println("CALCULADORA CIFP\n" + "------------------------------\n"
					+ "1.Suma\n2.Resta\n3.Multiplicaci�n\n4.Divisi�n\n0.Salir");
			operacion = t.nextInt();
		}
		t.close();

	}

}
