package unidad3;

import java.util.Scanner;

/*5. En el m�todo main de una clase Java llamada Multiplicar escribe un programa 
que sirva de ayuda para que los ni�os aprendan a multiplicar. El programa debe 
preguntarle al ni�o que tabla quiere repasar pidi�ndole un n�mero entre 1 y 9. 
Si el ni�o no responde con un n�mero entre 1 y 9 el programa deber� repetir la 
pregunta. A continuaci�n, el programa le ir� preguntando uno a uno el resultado 
de cada multiplicaci�n de la tabla elegida, mostrando en caso de fallo la respuesta
correcta y contabilizando el error. Al final el programa le dir� a ni�o si est� 
aprobado (n�mero de fallos menor que 2) o est� suspenso (n�mero de fallos mayor 
o igual que dos). Despu�s de esto el programa le preguntar� al ni�o si desea 
repasar una tabla de nuevo. Si contesta afirmativamente se repite el proceso, 
sino el programa finaliza.**/

public class Multiplicar {

	public static void main(String[] args) {

		Scanner t = new Scanner(System.in);
		int n, cont;

		System.out.println("Desea aprender a multiplicar? si/no");

		while (t.next().equals("si")) {
			cont = 0;
			do {

				System.out.println("Introduzca el n�mero de la tabla del 1 al 9");
				n = t.nextInt();

			} while (n < 1 || n > 9);

			for (int i = 0; i < 10; i++) {

				System.out.printf("%d x %d = ", n, i);

				if (t.nextInt() != (n * i)) {
					cont++;
					System.out.printf("ERROR, el resultado correcto es, %d \n", (n * i));
				}
			}

			if (cont < 2)
				System.out.println("Est�s aprobado");
			else
				System.out.println("Suspenso");

			System.out.println("Desea continuar?");
		}
		t.close();

	}

}
