package unidad3;

import java.util.Random;
import java.util.Scanner;

/*7. En el m�todo main de una clase llamada AdivinaNumero escribe un programa
para jugar a un juego de adivinaci�n. El juego consiste en que el ordenador
genera un n�mero aleatorio entre 1 y N, ambos incluidos, siendo N un n�mero
entero mayor o igual que 1.000 y menor o igual que 100.000, que tambi�n
elegir� de forma aleatoria. Una vez elegidos, el ordenador mostrar� el mensaje
�He pensado un n�mero entre 1 y N, adivina cu�l es: �. El usuario tendr� que
introducir n�meros hasta que acierte el que ha pensado el ordenador. Cada vez
que introduzca un n�mero y no acierte el ordenador le dir� si es mayor o menor
que el que ha pensado y le volver� a preguntar.
**/
public class AdivinaNumero {

	public static void main(String[] args) {
		// Clase Random
		Scanner t = new Scanner(System.in);
		Random r = new Random();
		// int max = r.nextInt(99001)+1000;
		//String jugar;
		//depurar entrada
		System.out.println("Desea jugar? (SI/NO)");
		//jugar = t.next();
		//while(jugar.equals("si")) La pondriamos abajo tambi�n este caso.
		//se hace con el next porque el next line lee el retorno de carro
		//while(t.next().equals("si"))
		while (t.next().equals("si")) {
			
			int max = r.nextInt(20) + 1;
			int n = r.nextInt(max) + 1;
			int numero = -1;

			do {
				System.out.println("He pensado un n�mero entre 1 y " + max + ", adivina cu�l es: ");

				numero = t.nextInt();

				if (numero > n)
					System.out.println("El numero es menor");
				else if (numero < n)
					System.out.println("El numero es mayor");

			} while (numero != n);

			System.out.println("Has acertado.\nDesea jugar otra vez?(0=No, 1=SI)");

		}
	}

}
