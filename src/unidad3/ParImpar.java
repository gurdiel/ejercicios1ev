package unidad3;

import java.util.Scanner;

/*1. En el m�todo main de una clase Java llamada ParImpar escribe un programa que diga si un
n�mero introducido por teclado es par o impar.
*/
public class ParImpar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Escriba n�mero para analizar.\n");
		int numero = teclado.nextInt();
		
		if(numero%2 == 0)
			System.out.println("El n�mero " + numero +" es par.");
		else
			System.out.println("El n�mero " + numero + " es impar.");

	}

}
