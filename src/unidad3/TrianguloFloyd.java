package unidad3;

import java.util.Scanner;

public class TrianguloFloyd {

	/*
	 * 9. En el m�todo main de una clase Java llamada TrianguloFloyd escribe un
	 * programa quegenere �l triangulo de Floyd para un n�mero de filas que
	 * introduciremos por teclado y lo muestre por pantalla. Por ejemplo, si el
	 * n�mero de filas es 4 el tri�ngulo de Floyd ser�: 1 2 3 4 5 6 7 8 9 10
	 * 
	 **/

	public static void main(String[] args) {

		Scanner t = new Scanner(System.in);
		System.out.println("Introduzca numero");
		int n = t.nextInt();

		for (int i = 1; i < n; i++) {

			for (int j = 1; j <= i; j++) {
				System.out.printf("%d", j);
			}
			System.out.println("");
		}

		t.close();
	}

}
