package unidad3;

import java.util.Random;
import java.util.Scanner;

/*10.En el m�todo main de una clase llamada Dado escribe un programa que simule
el lanzamiento de un dado N veces, siendo N un n�mero entero que se
introducir� por teclado. El programa finalizar� mostrando en la pantalla cuantas
veces sali� cada una de las caras.**/

public class Dado {

	public static void main(String[] args) {

		Random dado = new Random();
		Scanner t = new Scanner(System.in);
		int n;
		int cont1 = 0, cont2 = 0, cont3 = 0, cont4 = 0, cont5 = 0, cont6 = 0;

		System.out.println("Desea hacer tiradas?? si/no");
		while (t.next().equals("si")) {
			System.out.println("Cuantas veces desea tirar el dado?");
			int numero = t.nextInt();
			for (int i = 0; i < numero; i++) {
				n = dado.nextInt(6) + 1;
				System.out.println(n);

				switch (n) {
				case 1:
					cont1++;
					break;
				case 2:
					cont2++;
					break;
				case 3:
					cont3++;
					break;
				case 4:
					cont4++;
					break;
				case 5:
					cont5++;
					break;
				case 6:
					cont6++;
					break;

				}

			}
			System.out.printf("Numero 1: %d\nNumero 2: %d\nNumero 3: %d\nNumero "
					+ "4: %d\n" + "Numero 5: %d\nNumero 6: %d\n",
					cont1, cont2, cont3, cont4, cont5, cont6);
			System.out.println("Desea volver a jugar? si/no");
		}
		t.close();
	}

}

/*
 * Finalmente, para conseguir un entero, quitamos los decimales usando la clase
 * Math.floor()
 */

/*
 * int valorDado = Math.floor(Math.random()*6+1); En general, para conseguir un
 * n�mero entero entre M y N con M menor que N y ambos inclu�dos, debemos usar
 * esta f�rmula
 * 
 * int valorEntero = Math.floor(Math.random()*(N-M+1)+M); // Valor entre M y N,
 * ambos incluidos.
 * 
 * MEJOR CON OBJETO RANDOM
 **/