package unidad3;

import java.util.Scanner;

public class Bisiesto {

	public static void main(String[] args) {
		
/*En el m�todo main de una clase Java llamada Bisiesto escribe un programa que 
utilizando una �nica expresi�n l�gica determine si un a�o es o no es 
bisiesto. El a�o se introducir� por teclado y el resultado lo mostrar� en la 
pantalla. Un a�o es bisiesto si es m�ltiplo de 4. Los a�os m�ltiplos de
100 no son bisiestos, salvo si son m�ltiplos de 400.*/
		Scanner t = new Scanner(System.in);
		
		for(int i=0;i<10;i++) {
			
		int anio= t.nextInt();
		if(((anio%4)==0) && (!(anio%100==0) || (anio%400==0)))
			
			System.out.println(anio+ " es bisiesto.");
		else
			System.out.println("No es bisiesto.");
		}
		t.close();
	}

}
